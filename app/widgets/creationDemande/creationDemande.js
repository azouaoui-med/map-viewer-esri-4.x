define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!app/widgets/creationDemande/creationDemande.html",
        "esri/layers/FeatureLayer",
        "esri/widgets/Sketch/SketchViewModel",
        "esri/layers/GraphicsLayer",
    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        FeatureLayer,
        SketchViewModel,
        GraphicsLayer
    ) {
        return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: template,

            startup () {
                this.initForm();
                this.initDraw();
            },
            initForm () {
                $("#demandeForm").steps({
                    headerTag: "h2",
                    bodyTag: "section",
                    transitionEffect: "slideLeft",
                    enableCancelButton: true,
                    labels: {
                        cancel: "Cancel",
                        current: "current step:",
                        pagination: "Pagination",
                        finish: "Terminer",
                        next: "Suivant",
                        previous: "Précédent",
                        loading: "Loading ..."
                    },
                    onStepChanging(event, currentIndex, newIndex) {
                        return $(this).valid();
                    }
                }).validate({
                    // Specify validation rules
                    excluded: ':disabled, :hidden, :not(:visible), [aria-hidden]',
                    rules: {
                        date_demande: "required",
                        date_propable: "required",
                        state: "required",
                        duree: {
                            required: true,
                            maxlength: 2
                        }
                    },
                    // Specify validation error messages
                    messages: {
                        date_demande: "Please enter your date demand",
                        date_propable: "Please enter your date propable",
                        state: "Please enter your date state",
                        duree: {
                            required: "Please provide a duree",
                            maxlength: "max 2"
                        },

                    }
                });


                var layer = new FeatureLayer({
                    url: 'http://10.6.99.187:6080/arcgis/rest/services/OufokMapSevices/D%C3%A9coupage_SDA/MapServer/6'
                });
                var query = layer.createQuery();
                query.where = "1=1";
                var data = [];
                layer.queryFeatures(query).then(response => {

                    data = response.features.map(feature => ({id:feature.attributes.nom_commune,text:feature.attributes.nom_commune}));
                        
                    $('#select_commune').select2({
                        width: '100%',
                        placeholder: "Séléctionner une commune ...",
                        data: data,
                        allowClear: true
                    });

                });

                $('#date_demande').datepicker({
                    endDate: new Date()
                });
                $('#date_propable').datepicker({
                    startDate: new Date()
                });

            },
            initDraw: function () {
                
                var tempGraphicsLayer = new GraphicsLayer();


                var view = this.mapView;
                this.mapView.map.add(tempGraphicsLayer);
                this.mapView.when(function () {

                    var sketchViewModel = new SketchViewModel({
                        view: view,
                        layer: tempGraphicsLayer,
                        pointSymbol: {
                            type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
                            style: "circle",
                            color: "red",
                            size: "16px",
                            outline: { // autocasts as new SimpleLineSymbol()
                                color: [255, 255, 0],
                                width: 3
                            }
                        },
                        polylineSymbol: {
                            type: "simple-line", // autocasts as new SimpleMarkerSymbol()
                            color: "#8A2BE2",
                            width: "2",
                            style: "solid"
                        },
                        polygonSymbol: {
                            type: "simple-fill", // autocasts as new SimpleMarkerSymbol()
                            color: "rgba(138,43,226, 0.8)",
                            style: "solid",
                            outline: { // autocasts as new SimpleLineSymbol()
                                color: "red",
                                width: 2
                            }
                        }
                    });
                    console.log("sketchViewModel");
                    console.log(sketchViewModel);
                    
                    
                    sketchViewModel.on("draw-complete", function (evt) {
                        console.log(evt);
                        console.log("tempGraphicsLayer");
                        console.log(tempGraphicsLayer);

                        // add the graphic to the graphics layer
                        tempGraphicsLayer.add(evt.graphic);
                        //setActiveButton();
                    });

                    $('#btn-add-point').click($.proxy(function (e) {
                        sketchViewModel.create("point");

                    }, this));
                    $('#btn-add-line').click($.proxy(function (e) {
                        sketchViewModel.create("polyline");
                    }, this));
                    $('#btn-add-polygon').click($.proxy(function (e) {
                        sketchViewModel.create("polygon");
                    }, this));
                    $('#btn-clear').click($.proxy(function (e) {
                        tempGraphicsLayer.removeAll();
                        sketchViewModel.reset();
                    }, this));

                });

            }

        });
    });