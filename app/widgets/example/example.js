define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!app/widgets/example/example.html",
        "esri/Basemap",
        "esri/widgets/Sketch/SketchViewModel",
        "esri/views/2d/draw/Draw",
        "esri/layers/GraphicsLayer",
        "esri/Graphic"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        Basemap,
        SketchViewModel,
        Draw,
        GraphicsLayer,
        Graphic
    ) {
        return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: template,

            startup: function () {

                $('#button-submit').click($.proxy(function (e) {
                    e.preventDefault();
                    console.log(this.mapView);
                    this.mapView.goTo({
                        center: [3.262939, 36.618283],
                        tilt: 10,
                        zoom: 10
                    })

                }, this));

                var view = this.mapView;

                $('#button-submit').click($.proxy(function (e) {

                    this.mapView.goTo({
                        center: [3.262939, 36.618283],
                        tilt: 10,
                        zoom: 10
                    })

                }, this));
                $('#button-basmap').click($.proxy(function (e) {
                    this.mapView.map.basemap = new Basemap({
                        baseLayers: [new MapImageLayer({
                            url: "http://10.6.99.187:6080/arcgis/rest/services/OufokMapSevices/FDP_1000_Fusion_wgs/MapServer"
                        })],
                        title: "FDP",
                        id: "FDP1000",
                        thumbnailUrl: "https://stamen-tiles.a.ssl.fastly.net/terrain/10/177/409.png"
                    });
                }, this));
                $('#button-stress').click($.proxy(function (e) {
                    this.mapView.map.basemap = 'streets';
                }, this));

                //Using draw tool


                var draw = new Draw({
                    view: view
                });

                $('#btn-draw-point').click($.proxy(function (e) {
                    var action = draw.create("point");

                    action.on("draw-complete", function (evt) {
                        // if multipoint geometry is created, then change the symbol
                        // for the graphic
                        console.log(evt);
                        var point = {
                            type: "point", // autocasts as /Point
                            x: evt.coordinates[0],
                            y: evt.coordinates[1],
                            spatialReference: view.spatialReference
                        };
                        var graphic = new Graphic({
                            geometry: point,
                            symbol: {
                                type: "simple-marker", // autocasts as SimpleMarkerSymbol
                                style: "square",
                                color: "red",
                                size: "16px",
                                outline: { // autocasts as SimpleLineSymbol
                                    color: [255, 255, 0],
                                    width: 3
                                }
                            }
                        });
                        // add the graphic to the graphics layer
                        view.graphics.add(graphic);
                        //setActiveButton();
                    });

                }, this));

               /* var datepicker = $('.input-group.date').datepicker({
                    startDate:new Date()
                });*/
                //datepicker.setStartDate(new Date());
            }

        });
    });