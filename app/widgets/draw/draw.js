define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!app/widgets/draw/draw.html",
        "dojo/i18n!app/widgets/draw/nls/local",
        "esri/layers/MapImageLayer",
        "esri/Basemap",
        "esri/widgets/Sketch/SketchViewModel",
        "esri/views/2d/draw/Draw",
        "esri/layers/GraphicsLayer",
        "esri/Graphic",
        "dojo/domReady!"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        i18n,
        MapImageLayer,
        Basemap,
        SketchViewModel,
        Draw,
        GraphicsLayer,
        Graphic
    ) {
        return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: template,
            i18n: i18n,

            startup: function () {


                var tempGraphicsLayer = new GraphicsLayer();


                var view = this.mapView;
                this.mapView.map.add(tempGraphicsLayer);
                var pointSymbol = { // symbol used for points
                    type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
                    style: "square",
                    color: "#8A2BE2",
                    size: "16px",
                    outline: { // autocasts as new SimpleLineSymbol()
                        color: [255, 255, 255],
                        width: 3 // points
                    }
                }
                var polylineSymbol = { // symbol used for polylines
                    type: "simple-line", // autocasts as new SimpleMarkerSymbol()
                    color: "#8A2BE2",
                    width: "4",
                    style: "dash"
                }

                var polygonSymbol = { // symbol used for polygons
                    type: "simple-fill", // autocasts as new SimpleMarkerSymbol()
                    color: "rgba(138,43,226, 0.8)",
                    style: "solid",
                    outline: {
                        color: "white",
                        width: 1
                    }
                }
                this.mapView.when(function () {

                    var sketchViewModel = new SketchViewModel({
                        view: view,
                        layer: tempGraphicsLayer,
                        pointSymbol: pointSymbol,
                        polylineSymbol: polylineSymbol,
                        polygonSymbol: polygonSymbol
                    });
                    sketchViewModel.on("draw-complete", function (evt) {
                        var geometry = evt.geometry;
                        var symbol;
              
                        // Choose a valid symbol based on return geometry
                        switch (geometry.type) {
                          case "point":
                            symbol = pointSymbol;
                            break;
                          case "polyline":
                            symbol = polylineSymbol;
                            break;
                          default:
                            symbol = polygonSymbol;
                            break;
                        }
                        // Create a new graphic; add it to the GraphicsLayer
                        var graphic = new Graphic({
                          geometry: geometry,
                          symbol: symbol
                        });

                        // add the graphic to the graphics layer
                        tempGraphicsLayer.add(graphic);
                        //setActiveButton();
                    });


                    $('#btn-sketch-point').click($.proxy(function (e) {
                        sketchViewModel.create("point");

                    }, this));
                    $('#btn-sketch-line').click($.proxy(function (e) {
                        sketchViewModel.create("polyline");
                        //Disable freehand-drawing
                        //sketchViewModel.draw.activeAction._dragEnabled = false;
                    }, this));
                    $('#btn-sketch-polygon').click($.proxy(function (e) {
                        sketchViewModel.create("polygon");
                    }, this));
                    $('#btn-sketch-reset').click($.proxy(function (e) {
                        tempGraphicsLayer.removeAll();
                        sketchViewModel.reset();
                    }, this));


                })




            }

        });
    });