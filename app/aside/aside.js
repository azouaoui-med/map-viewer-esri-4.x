define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!app/aside/aside.html",
        "config/mapConfig",
        "esri/widgets/BasemapGallery",
        "esri/widgets/BasemapGallery/support/LocalBasemapsSource",
        "esri/widgets/Legend",
        "esri/widgets/LayerList",
        "dojo/domReady!"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        mapConfig,
        BasemapGallery,
        LocalBasemapsSource,
        Legend,
        LayerList

    ) {
        return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: template,
            startup() {

                var basemapContainer = document.createElement("div");
                $(this.domNode).find("#basemapGallery").append(basemapContainer);
                
                var localSource = new LocalBasemapsSource({
                    basemaps: mapConfig.localBasemaps
                });
                var basemapGallery = new BasemapGallery({
                    view: this.mapView,
                    container: basemapContainer,
                    source: localSource
                });

                basemapGallery.on("error", msg => console.log("basemap gallery error:  ", msg));

                $.ajax({
                    url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer"
                }).done(() =>
                    mapConfig.onlineBasemaps.forEach((basemap) => localSource.basemaps.push(basemap))
                );

                var legendContainer = document.createElement('div');
                $(this.domNode).find("#legend").append(legendContainer);

                var legend = new Legend({
                    view: this.mapView,
                    container: legendContainer
                });
                var layers = document.createElement("div");
                $(this.domNode).find("#layerList").append(layers);
                var layerList = new LayerList({
                    view: this.mapView,
                    container: layers,
                });

            }
        });
    });