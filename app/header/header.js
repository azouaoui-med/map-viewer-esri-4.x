define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!app/header/header.html",
        "dojo/i18n!app/header/nls/local",
        "dojo/domReady!"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        i18n

    ) {
        return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: template,
            i18n: i18n,
            startup() {

                $(this.domNode).find('#toggleBaseGallery').click(e => {
                    e.preventDefault();
                    $(".sideBar").toggleClass("hidden");
                });

                $(this.domNode).find('#changeLanguage').click((e) => {
                    e.preventDefault();

                    localStorage.setItem('locale', $(e.currentTarget).attr("data-language"));
                    location.reload();
                });

                //document.getElementById('fullScreen').dispatchEvent(new KeyboardEvent('keypress',{'key':'F11'}));
                $(this.domNode).find('#fullScreen').click(e => {
                    e.preventDefault();

                    var elem = document.body; // Make the body go full screen.
                    this.toggleFullscreen(elem);

                });


            },
            toggleFullscreen(elem) {
                elem = elem || document.documentElement;
                if (!document.fullscreenElement && !document.mozFullScreenElement &&
                    !document.webkitFullscreenElement && !document.msFullscreenElement) {
                    if (elem.requestFullscreen) {
                        elem.requestFullscreen();
                    } else if (elem.msRequestFullscreen) {
                        elem.msRequestFullscreen();
                    } else if (elem.mozRequestFullScreen) {
                        elem.mozRequestFullScreen();
                    } else if (elem.webkitRequestFullscreen) {
                        elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                } else {
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if (document.msExitFullscreen) {
                        document.msExitFullscreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    }
                }
            }
        });
    });