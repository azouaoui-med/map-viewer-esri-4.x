define([
    "dojo/i18n!config/nls/local",
], i18n => {

    return {
        menus: [{
                title: "Example",
                type: 'simple',
                icon: '',
                widget: {
                    title: 'Widget title',
                    icon: '<i class="fa fa-clone" aria-hidden="true"></i>',
                    path: 'app/widgets/example/example'
                }
            },
            {
                title: i18n.drawTitle,
                type: 'simple',
                icon: '',
                widget: {
                    title: i18n.drawTitle,
                    icon: '<i class="fa fa-pencil-alt" aria-hidden="true"></i>',
                    path: 'app/widgets/draw/draw'
                }
            },
            {
                title: i18n.demand,
                type: 'dorpdown',
                icon: '',
                submenus: [{
                    title: i18n.creationDemand,
                    icon: '',
                    widget: {
                        title: i18n.creationDemand,
                        icon: '<i class="far fa-folder-open"></i>',
                        path: 'app/widgets/creationDemande/creationDemande'
                    }
                }]
            }
        ]

    }

});