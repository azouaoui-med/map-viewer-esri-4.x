define({
    root: {
        drawTitle: "Draw",
        demand:"Demandes",
        creationDemand :"Création demandes",
        exampleTitle:"Exemple",
        drawTitleMenu :"Dessin",
        drawTitle :"Exemple Dessin",
        algeria:"Carte d'Algérie",
        satellite:"World Imagery",
        streets:"World Street Map",
        osm :"Open Street Map",
        commune:"Commune",
        wilaya:"Wilaya"
    },
    "ar": true
});