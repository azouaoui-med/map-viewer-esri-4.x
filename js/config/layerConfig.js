define([
    "esri/layers/FeatureLayer"
    
], (FeatureLayer) => {

    return {
        layers: [
            new FeatureLayer({
                url: 'http://10.5.28.48:6080/arcgis/rest/services/viewer/mxd_viewer/FeatureServer/0',
                title:'test',
                popupEnabled:true,
                outFields:['*'],
                popupTemplate:{
                    title: "title: {objectid}",
                    content:"{objectid}"
                }
            })

        ]

    }
});