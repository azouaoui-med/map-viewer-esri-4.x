define([
    "dojo/i18n!config/nls/local",
    "esri/layers/FeatureLayer",

], (i18n, FeatureLayer) => ({
    // the feature layers needs to be published in 10.3 or higher version of arcgis server
    sources: [{
            featureLayer: new FeatureLayer({
                url: "http://10.5.28.224:6080/arcgis/rest/services/FDP/LP10042014_WGS84/MapServer/20",
                outFields: ["*"]
            }),
            searchFields: ["nom"],
            displayField: "nom",
            exactMatch: false,
            outFields: ["*"],
            name: i18n.commune,
            placeholder: i18n.commune,
            maxResults: 4,
            maxSuggestions: 4,
            suggestionsEnabled: true,
            minSuggestCharacters: 0
        },
        {
            featureLayer: new FeatureLayer({
                url: "http://10.5.28.224:6080/arcgis/rest/services/FDP/LP10042014_WGS84/MapServer/21",
                outFields: ["*"]
            }),
            searchFields: ["WILAYA"],
            displayField: "WILAYA",
            exactMatch: false,
            outFields: ["*"],
            name: i18n.wilaya,
            placeholder: i18n.wilaya,
            maxResults: 4,
            maxSuggestions: 4,
            suggestionsEnabled: true,
            minSuggestCharacters: 0
        }
    ]


}));