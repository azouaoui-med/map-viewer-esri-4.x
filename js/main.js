define([
        "js/initWidgets",
        "config/mapConfig",
        "config/layerConfig",
        "js/loader",
        "esri/widgets/ScaleBar",
        "esri/widgets/Fullscreen",
        "esri/widgets/Search",
        "config/searchConfig",
        "dojo/domReady!"

    ], (initWidgets, mapConfig, layerConfig, loader, ScaleBar, Fullscreen, Search, searchConfig) =>

    ({
        mapView: null,
        startup() {

            this.initLanguage();

            this.mapView = mapConfig.mapView;

            this.mapView.when(() => {

                this.mapView.map.addMany(layerConfig.layers);

                // Scalebar
                let scaleBar = new ScaleBar({
                    view: this.mapView
                });
                this.mapView.ui.add(scaleBar, "bottom-right");

                // search
                let searchWidget = new Search({
                    view: this.mapView,
                    sources: searchConfig.sources
                });
                this.mapView.ui.add(searchWidget, "top-right");

                //full Screen map
                fullscreen = new Fullscreen({
                    view: this.mapView
                });
                this.mapView.ui.add(fullscreen, "top-right");

            });

            initWidgets.startup(this.mapView);

            $(window).on('load', () => {
                loader.windowLoaded = true;
                if (loader.appLoaded) {
                    loader.onLoad();
                }
            });

        },
        initLanguage() {

            if (localStorage.getItem('locale') == 'ar') {
                $('body').addClass('rightToLeft');

            } else {
                $('body').removeClass('rightToLeft');
            }
        }
    })
);