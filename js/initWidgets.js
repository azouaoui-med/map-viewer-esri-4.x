define([
    "require",
    "config/widgetConfig",
    "js/loader",
    "dojo/domReady!"
], function (require, widgetConfig, loader) {

    return {
        endLoop: false,
        startup (view) {

            require(["app/header/header", "app/aside/aside", "app/widgets/widgetContainer"], $.proxy(function (header, aside, widgetContainer) {

                // create header
                let headerWidget = new header();
                headerWidget.mapView = view;
                let headerNode = $(headerWidget.domNode);
                $('#header').append(headerNode);
                headerWidget.startup();

                // create aside (sidbar)
                let asideWidget = new aside();
                asideWidget.mapView = view;
                let asideNode = $(asideWidget.domNode);
                $('#main').append(asideNode);
                asideWidget.startup();

                //loop throw widgetConfig and create widgets 
                for (let i = 0; i < widgetConfig.menus.length; i++) {

                    if (i >= (widgetConfig.menus.length - 1)) {
                        this.endLoop = true;
                    }

                    if (widgetConfig.menus[i].type == 'simple') {
                        //case of widget with a simple menu
                        this.simpleMenuWidget(widgetConfig.menus[i], widgetContainer, view);

                    } else if (widgetConfig.menus[i].type == 'dorpdown') {
                        //case of widget with a dropdown menu
                        this.dropdownMenuWidget(widgetConfig.menus[i], widgetContainer, view);
                    }

                }

            }, this));
        },
        simpleMenuWidget (menuConfig, widgetContainer, view) {
            var menu = $('<li/>', {
                class: 'nav-item',
                html: '<a class="nav-link" href="#">' + menuConfig.icon + menuConfig.title + '</a>'

            }).appendTo('ul#menuList');
            require([menuConfig.widget.path], widget => {

                var widgetContainerCons = new widgetContainer();
                var widgetContainerNode = $(widgetContainerCons.domNode);
                widgetContainerNode.find('.widgetTitle .widgetIcon')[0].innerHTML = menuConfig.widget.icon;
                widgetContainerNode.find('.widgetTitle .widgetText')[0].innerHTML = menuConfig.widget.title;

                var widgetCons = new widget();
                var widgetNode = $(widgetCons.domNode);

                widgetContainerNode.find('.widgetBody').append(widgetNode);

                $('#main').append(widgetContainerNode);

                menu.click((e) => {
                    e.preventDefault();

                    $(widgetContainerCons.domNode).show();
                    if (widgetContainerCons.minimizedWidget) {
                        widgetContainerCons.restoreWidget();
                    }
                    $('.widgetContainer').css('z-index', 40);
                    $(widgetContainerCons.domNode).css('z-index', 50);
                });

                widgetCons.mapView = view;
                widgetCons.startup();
                widgetContainerCons.startup();

                // check if all widget are loaded and run onload function
                if (this.endLoop) {
                    loader.appLoaded = true
                    if (loader.windowLoaded) {
                        loader.onLoad();
                    }

                }
            });
        },
        dropdownMenuWidget (menuConfig, widgetContainer, view) {

            let dropdown = $('<li/>', {
                class: 'nav-item dropdown',
                html: '<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + menuConfig.icon + menuConfig.title + '</a>'

            }).appendTo('ul#menuList');

            let dropdownMenu = $('<div/>', {
                class: 'dropdown-menu',
                "aria-labelledby": "navbarDropdown",

            }).appendTo(dropdown);

            //loop throw submenus and create widgets
            for (let i = 0; i < menuConfig.submenus.length; i++) {

                require([menuConfig.submenus[i].widget.path],widget => {

                    let widgetContainerCons = new widgetContainer();
                    let widgetContainerNode = $(widgetContainerCons.domNode);
                    widgetContainerNode.find('.widgetTitle .widgetIcon')[0].innerHTML = menuConfig.submenus[i].widget.icon;
                    widgetContainerNode.find('.widgetTitle .widgetText')[0].innerHTML = menuConfig.submenus[i].widget.title;

                    let widgetCons = new widget();
                    let widgetNode = $(widgetCons.domNode);

                    widgetContainerNode.find('.widgetBody').append(widgetNode);

                    $('#main').append(widgetContainerNode);

                    let menu = $('<a/>', {
                        class: 'dropdown-item',
                        href: "#",
                        html: menuConfig.submenus[i].icon + menuConfig.submenus[i].title

                    }).appendTo(dropdownMenu);

                    menu.click(e => {
                        e.preventDefault();
                        $(widgetContainerCons.domNode).show();
                        if (widgetContainerCons.minimizedWidget) {
                            widgetContainerCons.restoreWidget();
                        }
                        $('.widgetContainer').css('z-index', 40);
                        $(widgetContainerCons.domNode).css('z-index', 50);
                    });
                    widgetCons.mapView = view;
                    widgetCons.startup();
                    widgetContainerCons.startup();
                    // check if all widget are loaded and run onload function

                    if (this.endLoop) {
                        if (i >= (menuConfig.submenus.length - 1)) {
                            loader.appLoaded = true;
                            if (loader.windowLoaded) {
                                loader.onLoad();
                            }
                        }
                    }
                });
            }

        }
    }


});